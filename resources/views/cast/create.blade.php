@extends('adminlte.master')

@section('content')

    <div class="ml-3 mb-3 mt-3 mr-3">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Tambah Data Cast</h3>
            </div>
            <div class="ml-3 mb-3 mt-3 mr-3">
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama', '') }}" placeholder="Masukkan Nama">
                    @error('nama')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" name="umur" id="umur" value="{{ old('umur', '') }}" placeholder="Masukkan Umur">
                    @error('umur')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" name="bio" id="bio" value="{{ old('bio', '') }}" placeholder="Masukkan Bio">
                    @error('bio')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
        </div>
        </div>
    </div>
@endsection