@extends('adminlte.master')

@section('content')


<div class="mx-3 my-3">
    <div class="card card-primary">
        <div class="mx-3 my-3">
            
            <p>Nama : {{$cast->nama}}</p>
            <p>Umur : {{$cast->umur}}</p>
            <p>Bio : {{$cast->bio}}</p>
            <div class="d-flex justify-content-end">
                <a href="/cast" class="btn btn-info">Back</a>
            </div>
            
        </div>
    </div>
</div>

@endsection