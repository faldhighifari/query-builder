@extends('adminlte.master')

@section('content')
        <div class="ml-3 mb-3 mt-3 mr-3">
            
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success')}}
                </div>
            @endif

            <div class="mb-3">
            <a href="/cast/create" class="btn btn-primary">Tambah</a>
            </div>

            <div class="card card-primary">
            <table class="table table-bordered">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Umur</th>
                    <th scope="col">Bio</th>
                    <th scope="col" class="d-flex justify-content-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->nama}}</td>
                            <td>{{$value->umur}}</td>
                            <td>{{$value->bio}}</td>
                            <td class="d-flex justify-content-center" >
                                <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm my-1 mx-1">Show</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-primary btn-sm my-1 mx-1">Edit</a>
                                <form action="/cast/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm my-1 mx-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td  colspan="5" align="center" >No data</td>
                        </tr>  
                    @endforelse              
                </tbody>
            </table>
        </div>
    </div>

@endsection